import { Injectable } from '@angular/core';
import {User} from "../../interfaces/user";
import {Provider} from "../../interfaces/provider";
import {Product} from "../../interfaces/product";
import {Company} from "../../interfaces/company";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  access_token = '';
  expires_in = 0;
  isLoggedIn = false;
  user:User=<User>{};
  users:User[]=[];
  providers:Provider[]=[];
  products:Product[]=[];
  companies:Company[]=[];

  constructor() { }

  setAccessToken(access_token: string){
    localStorage.setItem('access_token', access_token);
    this.access_token = access_token;
  }

  getAccessToken(): string{
    if(this.access_token == ''){
      return <string>localStorage.getItem('access_token');
    } else {
      return this.access_token;
    }
  }

  setExpires(expires_in: number){
    localStorage.setItem('expires_in', expires_in.toString());
    this.expires_in = expires_in;
  }

  getExpires(): number{
    if(this.expires_in == 0){
      return parseInt(<string>localStorage.getItem('expires_in'));
    } else {
      return this.expires_in;
    }
  }

  setIsLoggedIn(isLoggedIn: boolean){
    localStorage.setItem('isLoggedIn', isLoggedIn.toString());
    this.isLoggedIn = isLoggedIn;
  }

  getIsLoggedIn(): boolean{
    if(!this.isLoggedIn){
      return  Boolean(<string>localStorage.getItem('isLoggedIn'));
    } else {
      return this.isLoggedIn;
    }
  }

  clean(){
    localStorage.clear();
    localStorage.setItem('isLoggedIn', 'false');
  }

  setUser(user: User){
    localStorage.setItem('user', JSON.stringify(user));
    this.user = user;
  }

  getUser(): User{
    if(Object.entries(this.user).length === 0){
      return  JSON.parse(<string>localStorage.getItem('user'));
    } else {
      return this.user;
    }
  }

  setUsers(users: User[]){
    localStorage.setItem('users', JSON.stringify(users));
    this.users = users;
  }

  getUsers(): User[]{
    if(this.users.length == 0){
      return  JSON.parse(<string>localStorage.getItem('users'));
    } else {
      return this.users;
    }
  }

  setProviders(providers: Provider[]){
    localStorage.setItem('providers', JSON.stringify(providers));
    this.providers = providers;
  }

  getProviders(): Provider[]{
    if(this.providers.length == 0){
      return  JSON.parse(<string>localStorage.getItem('providers'));
    } else {
      return this.providers;
    }
  }

  setProducts(products: Product[]){
    localStorage.setItem('products', JSON.stringify(products));
    this.products = products;
  }

  getProducts(): Product[]{
    if(this.products.length == 0){
      return  JSON.parse(<string>localStorage.getItem('products'));
    } else {
      return this.products;
    }
  }


  setCompanies(companies: Company[]){
    localStorage.setItem('companies', JSON.stringify(companies));
    this.companies = companies;
  }

  getCompanies(): Company[]{
    if(this.companies.length == 0){
      return  JSON.parse(<string>localStorage.getItem('companies'));
    } else {
      return this.companies;
    }
  }

}
