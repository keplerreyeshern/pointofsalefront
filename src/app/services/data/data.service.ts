import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from "./storage.service";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseUrl = environment.baseUrl + '/api';
  access_token: string = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.access_token = this.storage.getAccessToken();
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getDataAdmin(){
    return this.http.get<any>(this.baseUrl + '/admin/data', {headers: this.headers});
  }

  getDataPublic(){
    return this.http.get<any>(this.baseUrl + '/public/data', {headers: this.headers});
  }
}
