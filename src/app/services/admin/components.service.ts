import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from "../data/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  baseUrl = environment.baseUrl + '/api/components';
  access_token: string = '';
  closeBar = false;
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.access_token = this.storage.getAccessToken();
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getData(){
    return this.http.get<any>(this.baseUrl);
  }

  setClick(type: string){
    return this.http.get<any>(this.baseUrl + '/clicks/' + type);
  }

  setClose(option: boolean){
    this.closeBar = option;
  }

  getClose(){
    return this.closeBar;
  }
}
