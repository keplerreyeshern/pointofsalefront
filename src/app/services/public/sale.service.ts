import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../data/storage.service";

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  url = environment.baseUrl + '/api/sale';
  access_token: string = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.access_token = this.storage.getAccessToken();
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  postSale(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  getSales(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  Cuts(type:string){
    return this.http.get<any>(this.url + '/create/' + type, {headers: this.headers});
  }

}
