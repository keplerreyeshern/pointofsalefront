import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from "../data/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  url = environment.baseUrl + '/api/providers';
  access_token: string = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.access_token = this.storage.getAccessToken();
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  activeProvider(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postProvider(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putProvider(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteProvider(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
