import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  open:boolean=false;

  constructor() {
  }

  setOpen(option:boolean){
    this.open = option;
  }

  getOpen(){
    return this.open;
  }


}
