import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from "../data/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url = environment.baseUrl + '/api/products';
  access_token: string = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.access_token = this.storage.getAccessToken();
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  activeProduct(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postProduct(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putProduct(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteProduct(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }

  buyProduct(id: any, params: any){
    return this.http.post<any>(this.url + '/buy/' + id, params, {headers: this.headers});
  }
}
