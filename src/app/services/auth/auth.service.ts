import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from "../../interfaces/user";
import {StorageService} from "../data/storage.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.baseUrl;
  public isLoggedIn  = false;
  user:User=<User>{};

  constructor(private http: HttpClient,
              private storage: StorageService) {
  }

  postToken(params: any){
    return this.http.post<any>(this.baseUrl + '/oauth/token', params);
  }

  getDataUser(params: any){
    const headers = new HttpHeaders({
      'Authorization': params,
    });
    return this.http.get<any>(this.baseUrl + '/api/user', {headers});
  }

  signIn() {
    this.storage.setIsLoggedIn(true);
    this.isLoggedIn = true;
  }

  getsign() {
    return this.isLoggedIn;
  }

  signOut() {
    this.storage.clean();
    this.isLoggedIn = false;
  }

  getProfile(){
    this.user = this.storage.getUser();
    return this.user.profile;
  }
}
