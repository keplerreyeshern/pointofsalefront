export interface Product {
  id: number;
  amount: number;
  total: number;
  name: string;
  code: string;
  slug: string;
  description: string;
  price: number;
  image: string;
  active: boolean;
  provider_id: number;
  company_id: number;
  amountTotal: number;
  amountExist: number;
}
