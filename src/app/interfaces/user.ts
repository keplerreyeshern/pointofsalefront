export interface User {
  id: number;
  name: string;
  email: string;
  email_verified_at: string;
  password: string;
  telephone: string;
  pass: string;
  profile: string;
  avatar: string;
  company_id: number;
  active: boolean;
  remember_token: string;
}
