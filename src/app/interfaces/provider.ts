export interface Provider {
  id: number;
  name: string;
  slug: string;
  description: string;
  image: string;
  active: boolean;
  company_id: number;
}
