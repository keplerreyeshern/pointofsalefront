export interface Sale {
  id: number;
  company_id: number;
  active: boolean;
  user_id: number;
  total: number;
  created_at: string;
}
