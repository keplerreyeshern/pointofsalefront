export interface Company {
  id: number;
  name: string;
  slug: string;
  logo: string;
  active: boolean;
}
