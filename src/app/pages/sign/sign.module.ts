import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignRoutingModule } from './sign-routing.module';
import { SignComponent } from './sign.component';
import { LoginComponent } from './login/login.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgxSpinnerModule} from "ngx-spinner";


@NgModule({
  declarations: [
    SignComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    SignRoutingModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule,
    NgxSpinnerModule
  ]
})
export class SignModule { }
