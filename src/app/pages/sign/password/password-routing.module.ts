import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PasswordComponent } from './password.component';
import { EmailComponent } from "./email/email.component";
import { FindComponent } from "./find/find.component";

const routes: Routes = [
  {
    path: '',
    component: PasswordComponent,
    children: [
      {
        path: 'recuperar',
        component: EmailComponent
      },
      {
        path: 'encontrar/:token',
        component: FindComponent
      },
      {
        path: '',
        redirectTo: 'recuperar',
        pathMatch: 'full'
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }
