import { Component, OnInit } from '@angular/core';
import { faUser, faLock, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../../../services/auth/auth.service";
import { Alert } from "../../../interfaces/alert";
import { StorageService } from "../../../services/data/storage.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  storage = '';
  url = '';
  access_token = '';
  user: any;
  alert:Alert = <Alert>{};
  type = 'password';

  constructor(private spinnerService: NgxSpinnerService,
              private authService: AuthService,
              private serviceData: StorageService,
              private router: Router,
              private title: Title) {
    this.title.setTitle('Entrar');
  }

  ngOnInit(): void {
    this.alert.type = 'danger';
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'SYmWc2sV9RxSkqiWAov38fqbCPHYFSxxG89xUn46',
      username: form.value.email,
      password: form.value.password,
    };
    this.authService.postToken(params).subscribe( response => {
      this.serviceData.setAccessToken('Bearer ' + response.access_token);
      this.serviceData.setExpires(response.expires_in);
      this.access('Bearer ' + response.access_token);
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        this.alert.message = 'Las credenciales no coinciden en la base de datos. Verifica y vuelve a intentar';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else if(err.status == 500){
        this.spinnerService.hide();
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else if(err.status == 404){
        this.spinnerService.hide();
        this.alert.message = '¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else {
        this.spinnerService.hide();
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      }
      console.log(err);
      this.spinnerService.hide();
    });
  }

  access(params: any){
    this.spinnerService.show();
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
      if (this.user.active){
        this.authService.signIn();
        this.serviceData.setUser(this.user);
        if(this.user.profile == 'super_admin'){
          this.router.navigateByUrl('/admin');
        } else {
          this.router.navigateByUrl('/public');
        }
      } else {
        this.serviceData.clean();
        this.alert.message = 'Tu usuario tiene bloqueado el acceso comunicate con el administrador';
        this.alert.type = 'danger';
        this.alert.active = true;
        this.setTime();
        this.authService.signOut();
      }
      this.spinnerService.hide();
    }, err => {
      if(err.status == 500){
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
        this.spinnerService.hide();
      } else if (err.status == 404) {
        this.alert.message = 'Las credenciales no conionciden con la base de datos';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.spinnerService.hide();
      } else {
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
        this.spinnerService.hide();
      }
      console.log(err);
    });
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
