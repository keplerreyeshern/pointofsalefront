import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorsRoutingModule } from './errors-routing.module';
import { ErrorsComponent } from './errors.component';
import { E404Component } from './e404/e404.component';
import { E402Component } from './e402/e402.component';


@NgModule({
  declarations: [
    ErrorsComponent,
    E404Component,
    E402Component
  ],
  imports: [
    CommonModule,
    ErrorsRoutingModule
  ]
})
export class ErrorsModule { }
