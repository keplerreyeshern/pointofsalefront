import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E402Component } from './e402.component';

describe('E402Component', () => {
  let component: E402Component;
  let fixture: ComponentFixture<E402Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E402Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E402Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
