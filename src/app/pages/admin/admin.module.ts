import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ComponentsModule } from "../../components/components.module";
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent
  ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        NgxSpinnerModule,
        ComponentsModule
    ]
})
export class AdminModule { }
