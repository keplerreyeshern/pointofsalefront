import {Component, HostListener, OnInit} from '@angular/core';
import { Alert} from "../../interfaces/alert";
import { DataService } from "../../services/data/data.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ComponentsService } from "../../services/admin/components.service";
import { Title } from "@angular/platform-browser";
import {StorageService} from "../../services/data/storage.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
    } else {
      this.serviceMenu.setClose(false);
    }
  };
  alert:Alert=<Alert>{};

  constructor(private title: Title,
              private serviceData: DataService,
              private loading: NgxSpinnerService,
              public serviceMenu: ComponentsService,
              private storage: StorageService) {
    this.title.setTitle('Administrador');
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.serviceData.getDataAdmin().subscribe( response => {
      this.storage.setUsers(response.users);
      this.storage.setCompanies(response.companies);
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.loading.hide();
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else if (err.status == 404) {
        this.loading.hide();
        this.alert.message = '¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else {
        this.loading.hide();
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      }
      console.log(err.status);
      console.log(err);
      this.loading.hide();
    });
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
