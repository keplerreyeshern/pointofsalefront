import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { environment } from "../../../../../environments/environment";
import { CompaniesService } from "../../../../services/admin/companies.service";
import { Company } from "../../../../interfaces/company";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  faHandPointer = faHandPointer;
  image:any;
  company:Company=<Company>{};
  imageInit:any;
  thumbnail:any;
  url_images = environment.baseUrl;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: CompaniesService,
              private router: Router,
              private storage: StorageService) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: number){
    let companies = this.storage.getCompanies();
    let index = companies.findIndex(item => item.id == id);
    this.company = companies[index];
    this.titleService.setTitle("Editar " + this.company.name);
    this.imageInit = this.company.logo;
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.putCompany(this.company.id, params).subscribe(response => {
      this.company = response;
      let companies = JSON.parse(<string>localStorage.getItem('companies'));
      let index = companies.findIndex((item: {id:number}) => item.id == this.company.id)
      companies[index] = this.company;
      this.storage.setCompanies(companies);
      this.router.navigateByUrl('/admin/companies');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      this.setTime();
      console.log(err);
      this.loading.hide();
    });
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }


  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
