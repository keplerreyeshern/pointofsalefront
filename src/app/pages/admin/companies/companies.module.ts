import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './companies.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsModule } from "../../../components/components.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule}  from "ngx-pagination";


@NgModule({
  declarations: [
    CompaniesComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
    imports: [
        CommonModule,
        CompaniesRoutingModule,
        FontAwesomeModule,
        ComponentsModule,
        NgbModule,
        FormsModule,
        NgxPaginationModule
    ]
})
export class CompaniesModule { }
