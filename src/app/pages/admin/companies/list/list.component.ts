import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { Company } from "../../../../interfaces/company";
import { CompaniesService } from "../../../../services/admin/companies.service";
import {StorageService} from "../../../../services/data/storage.service";
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  companies: Company[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: CompaniesService,
              private storage: StorageService) {
    this.titleService.setTitle("Lista de Compañias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.companies = this.storage.getCompanies();
  }

  active(company: number){
    this.loading.show();
    this.service.activeCompany(company).subscribe( response => {
      const company = response;
      const index = this.companies.findIndex(item => item.id == company.id);
      this.companies[index].active = response.active;
      localStorage.setItem('companies', JSON.stringify(this.companies));
      if (response.active){
        this.alert = {
          type: 'success',
          message: 'La compañia se activo correctamente',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'La compañia se desactivo correctamente',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  delete(company: number){
    this.loading.show();
    this.service.deleteCompany(company).subscribe( response => {
      const index = this.companies.findIndex(item => item.id == response.id);
      this.companies.splice(index, 1);
      localStorage.setItem('companies', JSON.stringify(this.companies));
      this.alert = {
        type: 'success',
        message: 'La compañia se elimino con exito',
        active: true
      };
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

}
