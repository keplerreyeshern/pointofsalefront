import { Component, OnInit } from '@angular/core';
import { Alert } from "../../interfaces/alert";
import { Title } from "@angular/platform-browser";
import { DataService } from "../../services/data/data.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ComponentsService } from "../../services/public/components.service";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";
import {StorageService} from "../../services/data/storage.service";

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.sass']
})
export class PublicComponent implements OnInit {

  alert:Alert=<Alert>{};
  expires_in = 0;
  showtime: any;

  constructor(private title: Title,
              private serviceData: DataService,
              private loading: NgxSpinnerService,
              public serviceMenu: ComponentsService,
              private storage: StorageService,
              private serviceAuth: AuthService,
              private router: Router) {
    this.title.setTitle('Punto de Venta');
  }

  ngOnInit(): void {
    this.expires_in = this.storage.getExpires();
    setInterval(() => {
      if (this.expires_in > 0){
        this.accountant();
      } else {
        this.serviceAuth.signOut();
        this.router.navigateByUrl('/login');
      }
    }, 1000);
    this.getData();
  }

  getData(){
    this.loading.show();
    this.serviceData.getDataPublic().subscribe( response => {
      this.storage.setUsers(response.users);
      this.storage.setProviders(response.providers);
      this.storage.setProducts(response.products);
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.loading.hide();
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else if (err.status == 404) {
        this.loading.hide();
        this.alert.message = '¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else {
        this.loading.hide();
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      }
      console.log(err.status);
      console.log(err);
      this.loading.hide();
    });
  }



  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

  accountant(){
    this.expires_in = this.expires_in - 1000;
    this.showtime = this.parseMillisecondsIntoReadableTime(this.expires_in);
    localStorage.setItem('expires_in', this.expires_in.toString());
  }

  parseMillisecondsIntoReadableTime(milliseconds: number){
//Get hours from milliseconds
    var hours = milliseconds / (1000*60*60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : "0" + absoluteHours;

//Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : "0" +  absoluteMinutes;

//Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : "0" + absoluteSeconds;


    // return h + ":" + m + ":" + s;
    return h + ":" + m;
  }

}
