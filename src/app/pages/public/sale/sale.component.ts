import { Component, OnInit } from '@angular/core';
import { faPlus, faSearch, faMinus, faTrashAlt, faEye, faDollarSign, faTrash } from "@fortawesome/free-solid-svg-icons";
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Alert } from "../../../interfaces/alert";
import { Product } from "../../../interfaces/product";
import { environment } from "../../../../environments/environment";
import { NgForm } from "@angular/forms";
import { SaleService } from "../../../services/public/sale.service";
import { NgxSpinnerService } from "ngx-spinner";
import {parse} from "@fortawesome/fontawesome-svg-core";

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.sass']
})
export class SaleComponent implements OnInit {

  faPlus = faPlus;
  faEye = faEye;
  faDollarSign = faDollarSign;
  faTrash = faTrash;
  faMinus = faMinus;
  faSearch = faSearch;
  faTrashAlt = faTrashAlt;
  alert:Alert=<Alert>{};
  product:Product=<Product>{};
  productShow:Product=<Product>{};
  productSearch:Product=<Product>{};
  productsSearch:Product[]=[];
  productCode:Product=<Product>{};
  sale:Product[]=[];
  url_images = environment.baseUrl;
  total:number = 0;
  public page: number | undefined;

  constructor(private modalService: NgbModal,
              private service: SaleService,
              private loading: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  open(content:any) {
    this.product = <Product>{};
    let products = JSON.parse(<string>localStorage.getItem('products'));
    this.product = products.find((item: {code:string}) => item.code == this.productCode.code);
    if (this.product){
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

      }, (reason) => {

      });
    } else {
      this.alert.active = true;
      this.alert.type = 'danger';
      this.alert.message = 'No existe el producto buscado';
    }
  }

  openSearch(content:any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-search'}).result.then((result) => {

    }, (reason) => {

    });
  }

  showProduct(content:any, id:number, amount:number) {
    this.productShow = <Product>{};
    let products = JSON.parse(<string>localStorage.getItem('products'));
    this.productShow = products.find((item: {id:number}) => item.id == id);
    if (this.productShow){
      this.productShow.amount = amount;
      this.productShow.total = this.productShow.price * amount;
      this.modalService.open(content, {ariaLabelledBy: 'modal-product'}).result.then((result) => {

      }, (reason) => {

      });
    } else {
      this.alert.active = true;
      this.alert.type = 'danger';
      this.alert.message = 'No existe el producto buscado';
    }
  }

  plus(){
    this.product = <Product>{};
    let amount = '1';
    let products = JSON.parse(<string>localStorage.getItem('products'));
    let index = this.productCode.code.indexOf('*');
    if (index >= 0){
      amount = this.productCode.code.substring(0, index);
    }
    let code = this.productCode.code.substr(index + 1);
    this.product = products.find((item: {code:string}) => item.code == code);
    if (this.product){
      if (this.product.amount < parseInt(amount)){
        this.alert.active = true;
        this.alert.type = 'danger';
        this.alert.message = 'La cantidad ingresada es mayor a la cantidad de stock que tiene el producto';
        this.setTime();
      } else {
        let indexE = this.sale.findIndex((item: {code:string})=> item.code == this.product.code);
        if (indexE < 0){
          this.product.amount = parseInt(amount);
          this.product.total = this.product.price * parseInt(amount);
          this.total = this.total + this.product.total;
          this.sale.push(this.product);
        } else {
          if (this.sale[indexE].amount + parseInt(amount) > this.product.amount){
            this.alert.active = true;
            this.alert.type = 'danger';
            this.alert.message = 'La cantidad ingresada es mayor a la cantidad de stock que tiene el producto';
            this.setTime();
          } else {
            this.sale[indexE].amount = parseInt(amount) + this.sale[indexE].amount;
            this.sale[indexE].total = this.sale[indexE].price * this.sale[indexE].amount;
            this.total = this.total + (this.sale[indexE].price * parseInt(amount));
          }
        }
      }
    } else {
      this.alert.active = true;
      this.alert.type = 'danger';
      this.alert.message = 'No existe el producto buscado';
    }
    this.product = <Product>{};
    this.productShow = <Product>{};
    this.productCode = <Product>{};
  }

  minAmount(id:number){
    let index = this.sale.findIndex((item: {id:number})=> item.id == id);
    if (this.sale[index].amount == 1){
      this.sale[index].amount = 1;
    } else {
      this.sale[index].amount = this.sale[index].amount -1;
      this.sale[index].total = this.sale[index].price * this.sale[index].amount;
    }
    this.total = this.total - this.sale[index].price;
    this.productShow = this.sale[index];
  }

  plusAmount(id:number){
    let index = this.sale.findIndex((item: {id:number})=> item.id == id);
    this.sale[index].amount = this.sale[index].amount + 1;
    this.sale[index].total = this.sale[index].price * this.sale[index].amount;
    this.productShow = this.sale[index];
    this.total = this.total + this.sale[index].price;
  }

  delete(id:number){
    let index = this.sale.findIndex((item: {id:number})=> item.id == id);
    this.total = this.total - this.sale[index].total;
    this.sale.splice(index, 1);
    this.productShow = <Product>{};
  }

  searchProducts(){
    let products = JSON.parse(<string>localStorage.getItem('products'));
    let expression = new RegExp(`${this.productSearch.name}.*`, "i");
    this.productsSearch = products.filter((item: {name:string}) => expression.test(item.name));
  }

  confirm(){
    let option = confirm('¿Estas seguro que deceas cobrar?');
    if (option){
      if (this.sale.length == 0){
        this.alert.type ='danger';
        this.alert.active = true;
        this.alert.message ='No hay ningun producto a cobrar';
      } else {
        this.loading.show();
        let params = new FormData();
        params.append('Content-Type', 'multipart/form-data');
        params.append('sale', JSON.stringify(this.sale));
        params.append('total', this.total.toString());
        this.service.postSale(params).subscribe(response => {
          console.log(response);
          this.sale=[];
          this.total=0;
          this.loading.hide();
        }, err => {
          if (err.status == 500) {
            this.alert.active = true;
            this.alert.type = 'danger';
            this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
          } else {
            this.alert.active = true;
            this.alert.type = 'danger';
            this.alert.message = 'Se detecto un error comunicate con el administrador';
          }
          this.setTime();
          console.log(err);
          this.loading.hide();
        });
      }
    }
  }

  clean(){
    this.alert={
      type: '',
      message: '',
      active:false
    };
    this.product={
      id: 0,
      amount: 0,
      total: 0,
      name: '',
      code: '',
      slug: '',
      description: '',
      price: 0,
      image: '',
      active: false,
      provider_id: 0,
      company_id: 0,
      amountTotal: 0,
      amountExist: 0
    };
    this.productShow={
      id: 0,
      amount: 0,
      total: 0,
      name: '',
      code: '',
      slug: '',
      description: '',
      price: 0,
      image: '',
      active: false,
      provider_id: 0,
      company_id: 0,
      amountTotal: 0,
      amountExist: 0
    };
    this.productSearch={
      id: 0,
      amount: 0,
      total: 0,
      name: '',
      code: '',
      slug: '',
      description: '',
      price: 0,
      image: '',
      active: false,
      provider_id: 0,
      company_id: 0,
      amountTotal: 0,
      amountExist: 0
    };
    this.productsSearch=[];
    this.productCode={
      id: 0,
      amount: 0,
      total: 0,
      name: '',
      code: '',
      slug: '',
      description: '',
      price: 0,
      image: '',
      active: false,
      provider_id: 0,
      company_id: 0,
      amountTotal: 0,
      amountExist: 0
    };
    this.sale=[];
  }
}
