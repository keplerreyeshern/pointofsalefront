import {Component, OnInit} from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { Product } from "../../../../interfaces/product";
import { NgxSpinnerService } from "ngx-spinner";
import { ProductsService } from "../../../../services/public/products.service";
import { environment } from "../../../../../environments/environment";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.sass']
})
export class BuyComponent implements OnInit {

  faTable = faTable;
  alert:Alert=<Alert>{};
  product:Product=<Product>{};
  amount:number = 0;
  url_images = environment.baseUrl;
  value = '';

  constructor(private loading: NgxSpinnerService,
              private service: ProductsService,
              private storage: StorageService) { }

  ngOnInit(): void {
  }

  search(item:any){
    let products = this.storage.getProducts();
    if (isNaN(item.value)){
      let expression = new RegExp(`${item.value}.*`, "i");
      let index = products.findIndex(element => expression.test(element.name));
      this.product = products[index];
    } else {
      let index = products.findIndex(element => element.code == item.value);
      this.product = products[index];
    }
    if (!this.isObjEmpty(this.product)){
      this.alert.type = 'danger';
      this.alert.active = true;
      this.alert.message = 'No se encontro resultados con los parametros ingresados';
      this.setTime();
    }
    this.value = '';
  }

  buy(){
    this.loading.show();
    let amount = this.product.amount + this.amount;
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('amount', amount.toString());
    this.service.buyProduct(this.product.id, params).subscribe(response => {
      let products = this.storage.getProducts();
      let index = products.findIndex(item => item.slug == response.slug);
      products[index].amount = response.amount;
      this.storage.setProducts(products);
      this.product = <Product>{};
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      this.setTime();
      console.log(err);
      this.loading.hide();
    });
  }

  isObjEmpty(obj: Product) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return true;
    }

    return false;
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
