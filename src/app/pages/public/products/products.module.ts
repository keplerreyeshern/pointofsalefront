import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsModule } from "../../../components/components.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule}  from "ngx-pagination";
import { BuyComponent } from './buy/buy.component';
import { InventoryComponent } from './inventory/inventory.component';


@NgModule({
  declarations: [
    ProductsComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    BuyComponent,
    InventoryComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FontAwesomeModule,
    ComponentsModule,
    NgbModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class ProductsModule { }
