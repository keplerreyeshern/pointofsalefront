import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { environment } from "../../../../../environments/environment";
import { ProductsService } from "../../../../services/public/products.service";
import { Provider } from "../../../../interfaces/provider";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  faHandPointer = faHandPointer;
  providers:Provider[]=[];
  image:any;
  imageInit:any;
  thumbnail:any;
  url_images = environment.baseUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: ProductsService,
              private router: Router,
              private storage: StorageService) {
    this.titleService.setTitle("Nuevo Producto");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.providers = this.storage.getProviders();
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('price', form.value.price);
    params.append('description', form.value.description);
    params.append('provider', form.value.provider);
    params.append('code', form.value.code);
    params.append('amount', form.value.amount);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.postProduct(params).subscribe(response => {
      let products = this.storage.getProducts();
      products.push(response);
      this.storage.setProducts(products);
      this.router.navigateByUrl('/public/products');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      this.setTime();
      console.log(err);
      this.loading.hide();
    });
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }


  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
