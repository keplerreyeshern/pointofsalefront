import { Component, OnInit } from '@angular/core';
import {Alert} from "../../../../interfaces/alert";
import {Sale} from "../../../../interfaces/sale";
import {User} from "../../../../interfaces/user";
import {SaleService} from "../../../../services/public/sale.service";
import {NgxSpinnerService} from "ngx-spinner";
import {Product} from "../../../../interfaces/product";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.sass']
})
export class InventoryComponent implements OnInit {

  sales:Sale[]=[];
  products:Product[]=[];
  total=0;
  alert:Alert=<Alert>{};
  user:User=<User>{};

  constructor(private service: SaleService,
              private loading: NgxSpinnerService,
              private storage: StorageService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.products = this.storage.getProducts();
    this.loading.show();
    this.user = this.storage.getUser();
    this.service.getSales().subscribe( response => {
      let sales:Sale[] = response.salesTotal;
      let products = response.products;
      this.sales = sales.filter(item => item.active == false);
      for (let p=0; this.products.length>p; p++){
        // for (let s=0; this.sales.length>s; s++){
          for (let k=0; products.length>k; k++){
            this.products[p].amountTotal = 0;
            if (this.products[p].id == products[k].product_id){
              this.products[p].amountTotal = products[k].amount + this.products[p].amountTotal;
            }
          // }
        }
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
