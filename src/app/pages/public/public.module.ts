import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { NgxSpinnerModule} from "ngx-spinner";
import { ComponentsModule } from "../../components/components.module";
import { SaleComponent } from './sale/sale.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import {NgxPaginationModule} from "ngx-pagination";


@NgModule({
  declarations: [
    PublicComponent,
    SaleComponent,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    FontAwesomeModule,
    NgbModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class PublicModule { }
