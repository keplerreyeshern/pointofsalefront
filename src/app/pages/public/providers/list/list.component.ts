import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { ProvidersService } from "../../../../services/public/providers.service";
import { Provider } from "../../../../interfaces/provider";
import {environment} from "../../../../../environments/environment";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  providers: Provider[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};
  url_images = environment.baseUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: ProvidersService,
              private storage: StorageService) {
    this.titleService.setTitle("Lista de Proveedores");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.providers = this.storage.getProviders();
  }

  active(provider: number){
    this.loading.show();
    this.service.activeProvider(provider).subscribe( response => {
      const index = this.providers.findIndex(item => item.id == provider);
      this.providers[index].active = response.active;
      this.storage.setProviders(this.providers);
      if (response.active){
        this.alert = {
          type: 'success',
          message: 'El proveedor se activo correctamente',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'El proveedor se desactivo correctamente',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  delete(provider: number){
    this.loading.show();
    this.service.deleteProvider(provider).subscribe( response => {
      const index = this.providers.findIndex(item => item.id == response.id);
      this.providers.splice(index, 1);
      this.storage.setProviders(this.providers);
      this.alert = {
        type: 'success',
        message: 'El proveedor se elimino con exito',
        active: true
      };
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
