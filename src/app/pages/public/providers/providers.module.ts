import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProvidersRoutingModule } from './providers-routing.module';
import { ProvidersComponent } from './providers.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ComponentsModule } from "../../../components/components.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule}  from "ngx-pagination";


@NgModule({
  declarations: [
    ProvidersComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    ProvidersRoutingModule,
    FontAwesomeModule,
    ComponentsModule,
    NgbModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class ProvidersModule { }
