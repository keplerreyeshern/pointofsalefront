import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { environment } from "../../../../../environments/environment";
import { ProvidersService } from "../../../../services/public/providers.service";
import { Provider } from "../../../../interfaces/provider";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  faHandPointer = faHandPointer;
  image:any;
  provider:Provider=<Provider>{};
  providers:Provider[]=[];
  imageInit:any;
  thumbnail:any;
  url_images = environment.baseUrl;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: ProvidersService,
              private router: Router,
              private storage: StorageService) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: number){
    this.providers = this.storage.getProviders();
    let index = this.providers.findIndex(item => item.id == id);
    this.provider = this.providers[index];
    this.titleService.setTitle("Editar " + this.provider.name);
    this.imageInit = this.provider.image;
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.putProvider(this.provider.id, params).subscribe(response => {
      let index = this.providers.findIndex((item: {id:number}) => item.id == this.provider.id)
      this.providers[index] = response;
      this.storage.setProviders(this.providers);
      this.router.navigateByUrl('/public/providers');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      this.setTime();
      console.log(err);
      this.loading.hide();
    });
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }


  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
