import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { UsersService } from "../../../../services/admin/users.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { User } from "../../../../interfaces/user";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  user:User=<User>{};
  users:User[]=[];
  faHandPointer = faHandPointer;
  type = 'password';

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private router: Router,
              private storage: StorageService) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.user = this.storage.getUser();
    this.users = this.storage.getUsers();
  }

  submit(form: NgForm){
    if (form.value.password.length < 8){
      this.alert.active = true;
      this.alert.message = 'La contraseña debe tener almenos 8 caracteres';
      this.setTime();
    } else if(form.value.password != form.value.pass) {
      this.alert.active = true;
      this.alert.message = 'Las contraseñas deben coincidir';
      this.setTime();
    } else if(form.value.profile == undefined){
      this.alert.active = true;
      this.alert.message = 'Debes seleccionar un perfil';
      this.setTime();
    } else {
      this. verify(form);
    }
  }

  verify(form: NgForm){
    let user = this.users.filter(item => item.email == form.value.email);
    if (user.length > 0) {
      this.alert.active = true;
      this.alert.message = 'El correo ya se encuentra en nuestra base de datos, intenta con otro correo valido';
      this.setTime();
    } else {
      this.store(form);
    }
  }

  store(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('email', form.value.email);
    params.append('password', form.value.password);
    params.append('profile', form.value.profile);
    params.append('company', this.user.company_id.toString());
    this.service.postUser(params).subscribe(response => {
      this.users.push(response);
      this.storage.setUsers(this.users);
      this.router.navigateByUrl('/public/users');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      this.setTime();
      console.log(err);
      this.loading.hide();
    });
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  eyePass(){
    if(this.type == 'password'){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
