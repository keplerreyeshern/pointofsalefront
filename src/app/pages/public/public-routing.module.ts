import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';
import { PublicGuard } from "../../guards/public.guard";
import { SaleComponent } from "./sale/sale.component";

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: 'sale',
        component: SaleComponent
      },
      {
        path: 'users',
        canActivate: [PublicGuard],
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'providers',
        canActivate:[PublicGuard],
        loadChildren: () => import('./providers/providers.module').then(m => m.ProvidersModule)
      },
      {
        path: 'products',
        canActivate:[PublicGuard],
        loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'cuts',
        loadChildren: () => import('./cuts/cuts.module').then(m => m.CutsModule)
      },
      {
        path: '',
        redirectTo: 'sale'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
