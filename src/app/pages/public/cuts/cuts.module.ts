import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CutsRoutingModule } from './cuts-routing.module';
import { CutsComponent } from './cuts.component';
import { PartialComponent } from './partial/partial.component';
import { TotalComponent } from './total/total.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ComponentsModule} from "../../../components/components.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [
    CutsComponent,
    PartialComponent,
    TotalComponent
  ],
  imports: [
    CommonModule,
    CutsRoutingModule,
    NgbModule,
    ComponentsModule,
    FontAwesomeModule
  ]
})
export class CutsModule { }
