import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CutsComponent } from './cuts.component';
import { PartialComponent } from "./partial/partial.component";
import { PublicGuard } from "../../../guards/public.guard";
import { TotalComponent } from "./total/total.component";

const routes: Routes = [
  {
    path: '',
    component: CutsComponent,
    children: [
      {
        path: 'partial',
        component: PartialComponent
      },
      {
        path: 'total',
        canActivate: [PublicGuard],
        component: TotalComponent
      },
      {
        path: '',
        redirectTo: 'partial'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CutsRoutingModule { }
