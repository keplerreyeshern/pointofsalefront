import { Component, OnInit } from '@angular/core';
import { faCashRegister } from "@fortawesome/free-solid-svg-icons";
import { SaleService } from "../../../../services/public/sale.service";
import { Sale } from "../../../../interfaces/sale";
import { NgxSpinnerService } from "ngx-spinner";
import { Alert } from "../../../../interfaces/alert";
import { User } from "../../../../interfaces/user";
import {StorageService} from "../../../../services/data/storage.service";

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.sass']
})
export class TotalComponent implements OnInit {

  faCashRegister = faCashRegister;
  sales:Sale[]=[];
  total=0;
  alert:Alert=<Alert>{};
  user:User=<User>{};
  users:User[]=[];

  constructor(private service: SaleService,
              private loading: NgxSpinnerService,
              private storage: StorageService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.user = this.storage.getUser();
    this.service.getSales().subscribe( response => {
      this.sales = response.salesTotal;
      for (let i=0; this.sales.length>i; i++){
        this.total = this.sales[i].total + this.total;
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  getUser(id: number){
    let users = this.storage.getUsers();
    let index = users.findIndex(item => item.id == id);
    let user = users[index];
    return user.name;
  }

  PartialCut(){
    this.loading.show();
    this.service.Cuts('total').subscribe( response => {
      this.sales = [];
      this.total = 0;
      this.getData();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

}
