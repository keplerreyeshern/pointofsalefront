import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { User } from "../../../interfaces/user";
import { ComponentsService } from "../../../services/public/components.service";
import {StorageService} from "../../../services/data/storage.service";

@Component({
  selector: 'app-navbar-public',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarPublicComponent implements OnInit {

  faBars = faBars;
  open = false;
  display = 'none';
  user:User=<User>{};

  constructor(public service: AuthService,
              public componentService: ComponentsService,
              private storage: StorageService,
              private router: Router) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.user = this.storage.getUser();
  }

  toogleSub(){
    if (this.display == 'none'){
      this.display = 'block';
    } else {
      this.display = 'none';
    }
  }

  show(){
    this.display = 'block';
  }

  hide(){
    this.display = 'none';
  }

  toogle(){
    if (this.open){
      this.open = false;
    } else {
      this.open = true;
    }
    this.componentService.setOpen(this.open);
  }

  sigout(){
    this.service.signOut();
    this.router.navigate(['/login']);
  }

}
