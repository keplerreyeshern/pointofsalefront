import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarAdminComponent } from './admin/navbar/navbar.component';
import { SidebarAdminComponent } from './admin/sidebar/sidebar.component';
import { BreadcrumbAdminComponent } from './admin/breadcrumb/breadcrumb.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { RouterModule } from "@angular/router";
import { NavbarPublicComponent } from "./public/navbar/navbar.component";
import { BreadcrumbPublicComponent } from "./public/breadcrumb/breadcrumb.component";



@NgModule({
  declarations: [
    NavbarAdminComponent,
    SidebarAdminComponent,
    BreadcrumbAdminComponent,
    NavbarPublicComponent,
    BreadcrumbPublicComponent
  ],
  exports: [
    SidebarAdminComponent,
    NavbarAdminComponent,
    BreadcrumbAdminComponent,
    NavbarPublicComponent,
    BreadcrumbPublicComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule
  ]
})
export class ComponentsModule { }
