import { Component, OnInit } from '@angular/core';
import { faTachometerAlt, faStore, faWarehouse, faUser, faImages, faNewspaper, faBlog, faUsers } from '@fortawesome/free-solid-svg-icons';
import {ComponentsService} from "../../../services/admin/components.service";

@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarAdminComponent implements OnInit {

  faTachometerAlt = faTachometerAlt;
  faUsers = faUsers;
  faNewspaper = faNewspaper;
  faBlog = faBlog;
  faStore = faStore;
  faWarehouse = faWarehouse;
  faUser = faUser;
  faImages = faImages;

  constructor(public serviceMenu: ComponentsService) { }

  ngOnInit(): void {
  }

}
